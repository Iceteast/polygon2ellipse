#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

import sys

from cfg import *
from PyQt5.QtWidgets import QApplication, QMainWindow, QHBoxLayout, \
    QDesktopWidget, QMessageBox, QWidget

from framework.MyWidget import MyWidget


class Frame(QMainWindow):
    # main framework
    def __init__(self):
        super().__init__()
        self.setWindowTitle(APP_NAME + VERSION)
        self.resize(SCREEN_WIDTH, SCREEN_HEIGHT)
        self.statbar = self.statusBar()
        self.statbar.showMessage('Ready')
        self.setCenter()
        widget = QWidget()
        self.setCentralWidget(widget)

        hbox = QHBoxLayout()
        # setMainMap
        self.mainMap = MyWidget()
        hbox.addWidget(self.mainMap)

        widget.setLayout(hbox)

        self.show()

    # set Form im Center
    def setCenter(self):

        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def closeEvent(self, QCloseEvent):
        reply = QMessageBox.question(self,
                                     APP_NAME + VERSION,
                                     "Are you sure to exit ?",
                                     QMessageBox.No | QMessageBox.Yes,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            QCloseEvent.accept()
        else:
            QCloseEvent.ignore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = Frame()
    sys.exit(app.exec_())
