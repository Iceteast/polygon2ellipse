#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

from enum import Enum
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QPen, QBrush, QFont

# VERSION
APP_NAME = 'Polygon to Ellipse'
VERSION = ' v0.1'

# OPTIONS
GRIDLINES = True

# DISPLAY
SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768

VERTICES_COUNT = 30

SLIDER_MIN = 1
SLIDER_MAX = 200

# DATA
COORDINATE_MIN = -400
COORDINATE_MAX = 400
WIDTH_MIN = 0
WIDTH_MAX = 200
HEIGHT_MIN = 0
HEIGHT_MAX = 300
ANGLE_MIN = 0
ANGLE_MAX = 180
DEMO_HEIGHT = 50

LINE_COLOR = (153, 204, 255)  # #99CCFF
POINT_COLOR = (0, 153, 102)  # #009966
GRID_COLOR = (183, 183, 183)

