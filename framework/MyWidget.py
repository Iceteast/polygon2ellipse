import sys

from PyQt5 import QtGui, QtWidgets
from PyQt5.QtGui import QPainterPath, QPainter
from numpy.random import uniform

import utils
from cfg import *


class MyWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.linePen = QtGui.QPen(utils.makeColor(LINE_COLOR))  # set lineColor
        self.linePen.setWidth(1)  # set lineWidth
        self.pointPen = QtGui.QPen(utils.makeColor(POINT_COLOR))  # set lineColor
        self.pointPen.setWidth(3)  # set lineWidth
        self.points = None
        self.game_over = False
        self.startTimer(30)

    def getRandomVertex(self):
        return self.width() / 2 + uniform(-self.width() / 2, self.width() / 2), \
               self.height() / 2 + uniform(-self.height() / 2, self.height() / 2)

    def createPoints(self):
        points = []

        if self.points is None:
            for i in range(VERTICES_COUNT):  # add the points of polygon
                points.append(self.getRandomVertex())
        else:
            if len(self.points) == 1:
                self.game_over = True
                return self.points
            for i in range(len(self.points)):
                points.append(utils.getMidPoint(self.points[i], self.points[i - 1]))
        return points

    def createPath(self):
        self.points = self.createPoints()
        path = QPainterPath()
        sx, sy = self.points[-1]
        path.moveTo(sx, sy)
        for p in self.points:  # add the points of polygon
            x, y = p
            path.lineTo(x, y)
        return path

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(self.linePen)
        painter.drawPath(self.createPath())
        painter.setPen(self.pointPen)
        for p in self.points:
            x, y = p
            painter.drawPoint(int(x), int(y))
        painter.end()

    def timerEvent(self, a0: 'QTimerEvent') -> None:
        self.repaint()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    widget = MyWidget()
    widget.show()

    sys.exit(app.exec_())
