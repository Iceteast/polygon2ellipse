# Polygon to Ellipse v0.1

## About

It is interesting to note that when we join the centres of each side of a polygon, we get a smaller and smoother polygon. After several iterations, this polygon becomes an ellipse asymptotically. That's the idea of this program.

It's a simple simulator written by PyQt5. It shows how a polygon becomes to ellipse.

## Environment

    PyCharm 2023.1.3 (Community Edition)
    Build #PC-231.9161.41, built on June 20, 2023
    Runtime version: 17.0.7+10-b829.16 amd64
    VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
    Windows 11.0
    GC: G1 Young Generation, G1 Old Generation
    Memory: 2020M
    Cores: 16

## Python Environment

    Python 3.9
    PyQt5 5.15.9

## Run

    ./Main.py

## TODO
    
- [ ] Automatic Zoom In